<?php
#Name:PNG To Ritchey Image Data v1
#Description:Convert PNG data to Ritchey Image Data v1 data. Returns Ritchey Image Data v1 data as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data will contain a field called "Colour", and it will have an RGB colour code as the value. Black pixels after the last line break can optionally be ignored non-existent pixels at image end.
#Arguments:'string' is a string containing the PNG image data. 'trim_nonexistent_pixels' (optional) indicates if rgb(0,0,0) pixels after the last line break should be included in the image. Excluding them is useful when converting back and forth since a PNG requires a complete x axis, but Ritchey Image Data doesn't. When converting from Ritchey Image Data to PNG the rest of the x axis must be colored with something, and black is a common padding colour. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,trim_nonexistent_pixels:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('png_to_ritchey_image_data_v1') === FALSE){
function png_to_ritchey_image_data_v1($string, $trim_nonexistent_pixels = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($trim_nonexistent_pixels === NULL){
		$trim_nonexistent_pixels = FALSE;
	} else if ($trim_nonexistent_pixels === TRUE){
		#Do Nothing
	} else if ($trim_nonexistent_pixels === FALSE){
		#Do Nothing
	} else {
		$errors[] = "trim_nonexistent_pixels";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert PNG data to image. Extract the colour of each pixel, and apply formatting. Apply image width (new lines). If requested, delete black pixels after last colour on last x-axis.]
	if (@empty($errors) === TRUE){
		###Convert PNG data to image
		$image = @imagecreatefromstring($string);
		###Determine image width and height
		$width = @imagesx($image);
		$height = @imagesy($image);
		###Get each pixel color as RGB, add formatting, and save to array
		$ritchey_image_data = array();
		$total_pixels = $width * $height;
		$pixel_x_coordinate = 0;
		$pixel_y_coordinate = 0;
		for ($i = 0; $i < $total_pixels; $i++) {
			$pixel_colour_index = @imagecolorat($image, $pixel_x_coordinate, $pixel_y_coordinate);
			$pixel_colour_rgb = @imagecolorsforindex($image, $pixel_colour_index);
			$ritchey_image_data[] = "[Colour:{$pixel_colour_rgb['red']},{$pixel_colour_rgb['green']},{$pixel_colour_rgb['blue']}.]";
			$pixel_x_coordinate++;
			if ($pixel_x_coordinate >= $width){
				$pixel_x_coordinate = 0;
				$pixel_y_coordinate++;
			}
		}
		###Prepare to add width (new lines)
		$ritchey_image_data = @array_chunk($ritchey_image_data, $width, true);
		foreach ($ritchey_image_data as &$value) {
			$value = @implode($value);
		}
		unset($value);
		###If applicable, discard non-existent pixels from last horizontal axis
		if ($trim_nonexistent_pixels === TRUE){
			$key = @count($ritchey_image_data);
			$key = $key - 1;
			$last_x_axis = $ritchey_image_data[$key];
			$last_x_axis = @str_replace('[', '', $last_x_axis);
			$last_x_axis = @str_replace(']', '', $last_x_axis);
			$last_x_axis = @str_replace('Colour:', '', $last_x_axis);
			$last_x_axis = @explode('.', $last_x_axis);
			$last_x_axis = @array_reverse($last_x_axis);
			foreach ($last_x_axis as &$value) {
				if ($value === '0,0,0'){
					$value = '';
				} else if ($value == ''){
					$value = '';
				} else {
					$value = "[Colour:{$value}.]";
				}
			}
			unset($value);
			$last_x_axis = @array_reverse($last_x_axis);
			$last_x_axis = @array_filter($last_x_axis);
			$last_x_axis = @implode($last_x_axis);
			$ritchey_image_data[$key] = $last_x_axis;
		}
		###Add width (new lines)
		$ritchey_image_data = @implode("\n", $ritchey_image_data);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('png_to_ritchey_image_data_v1_format_error') === FALSE){
				function png_to_ritchey_image_data_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("png_to_ritchey_image_data_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $ritchey_image_data;
	} else {
		return FALSE;
	}
}
}
?>